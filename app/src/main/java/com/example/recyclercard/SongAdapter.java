package com.example.recyclercard;

import android.media.Image;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclercard.R;

import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder> {
    private List<Song> songList;




    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRank, tvSongName, tvSinger, tvYear;
        public ImageView tvAlbumCover;

        public ViewHolder(View v) {
            super(v);
            tvRank = v.findViewById(R.id.tv_rank);
            tvSongName = v.findViewById(R.id.tv_song_name);
            tvSinger = v.findViewById(R.id.tv_singer);
            tvYear = v.findViewById(R.id.tv_year);
            tvAlbumCover = v.findViewById(R.id.tv_album_cover);
        }
    }
      public SongAdapter(List<Song> songList){
          this.songList = songList;
      }
        @NonNull
        @Override
        public SongAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_song_list_item,parent,false);

            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Song song = songList.get(position);
            holder.tvRank.setText(String.valueOf(song.getRank()));
            holder.tvSongName.setText(String.valueOf(song.getName()));
            holder.tvSinger.setText(String.valueOf(song.getSinger()));
            holder.tvYear.setText(String.valueOf(song.getYear()));
            holder.tvAlbumCover.setImageResource(song.getPic());
            holder.tvYear.setText("2016");
        }

        @Override
        public int getItemCount() {
            return songList.size();
        }
    }

